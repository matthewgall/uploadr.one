import { Database } from "@cloudflare/d1";
import { Router } from 'itty-router';
import { nanoid } from 'nanoid';
import CFAccess from "@matthewgall/cfaccess-jwt";
import isAlphanumeric from 'validator/lib/isAlphanumeric';
import isLength from 'validator/lib/isLength';
// Templates
import indexTemplate from '../html/index.html';
import jsTemplate from '../html/app.js.txt';

const router = Router();

const b64toArrayBuffer = (dataURI: string) => {
    var byteString = atob(dataURI.split(',')[1]);
    var len = byteString.length;
    var bytes = new Uint8Array( len );
    for (var i = 0; i < len; i++)        {
        bytes[i] = byteString.charCodeAt(i);
    }
    return bytes.buffer;
}

const arrayBufferTob64 = (buf) => {
    let string = '';
    (new Uint8Array(buf)).forEach(
      (byte) => { string += String.fromCharCode(byte) }
    )
    return btoa(string)
}

const shuffleArray = (array) => {
    let m = array.length, t, i;
    while (m) {
      i = Math.floor(Math.random() * m--);
      t = array[m];
      array[m] = array[i];
      array[i] = t;
    }
    return array;
}

const genId = () => ('000000' + (Math.random() * Math.pow(36, 6) << 0).toString(36)).slice(-6)

router.get('/admin/initialize', async (request, env, context) => {
    try {
        let access = new CFAccess(env.ACCESS_DOMAIN, env.ACCESS_AUD);
        access = await access.validate(request);
    
        if (!access.valid) return new Response("Unauthorized", { status: 403 });
    }
    catch(e) { return new Response(e, { status: 403 }); }

    try {

        // First, we add a guard to stop people re-initializing if there are images
        try {
            let images: any = env.D1.prepare('SELECT COUNT(*) AS total FROM images');
            images = await images.first('total');

            if (images > 0) return new Response("Your instance has images uploaded. Add ?force=1 to confirm deletion", { status: 500 });
        }
        catch(e) {}
        
        await env.D1.batch([
            env.D1.prepare("DROP TABLE IF EXISTS images"),
            env.D1.prepare("DROP TABLE IF EXISTS images_blob"),
            env.D1.prepare("CREATE TABLE images (uid STRING PRIMARY KEY, name STRING, source STRING, storageKeys STRING)"),
            env.D1.prepare("CREATE TABLE images_blob (uid STRING PRIMARY KEY, data STRING)")
        ]);
        return new Response("System successfully initialised, database has been seeded.");
    }
    catch(e) {
        return new Response(e, { status: 403 });
    }
});

router.get('/admin/delete/:id.:format?', async (request, env, context) => {
    let { id, format = 'png' } = request.params;

    let resp = {
        'success': false
    }

    try {
        let access = new CFAccess(env.ACCESS_DOMAIN, env.ACCESS_AUD);
        access = await access.validate(request);
    
        if (!access.valid) return new Response("Unauthorized", { status: 403 });
    }
    catch(e) { return new Response(e, { status: 403 }); }

    // Validate the provided ID, trust no-one
    if (!isAlphanumeric(id) || !isLength(id, {min: 6, max: 12})) {
        resp['message'] = "Image not found, so not deleted";
        return new Response(JSON.stringify(resp), { headers: { 'Content-Type': 'application/json' } });
    }

    // Now, as a little bit of a trick, we're going to pad the field to allow defeat legal-blocks
    if (id.length > 6) id = id.slice(-6);

    // Now to find the image in D1
    try {
        let images: any = env.D1.prepare('SELECT COUNT(*) AS total FROM images');
        images = await images.first('total');

        if (images == 0) {
            return new Response("Image not found", { status: 404 });
        } 
    }
    catch(e) {}

    // Now we know it exists, so fetch it
    let image: any = env.D1.prepare('SELECT name, storageKeys FROM images WHERE uid=?').bind(id);
    image = await image.all();
    let storedData = JSON.parse(image.results[0].storageKeys);
    
    for (let s of Object.keys(storedData)) {
        try {
            if (s == "sql") {
                await env.D1.batch([
                    env.D1.prepare("DELETE FROM images_blob WHERE uid = ?").bind(storedData.sql)
                ]);
            }
            if (s == "r2") await env.R2.delete(storedData.r2);
            if (s == "kv") await env.KV.delete(storedData.kv);
        }
        catch(e) {}
    }

    // Finally we delete the image's record
    await env.D1.batch([
        env.D1.prepare("DELETE FROM images WHERE uid = ?").bind(id)
    ]);
  

    resp['success'] = true;
    resp['message'] = "Image deleted successfully";

    return new Response(JSON.stringify(resp), { headers: { 'Content-Type': 'application/json' } });
});

router.post('/upload', async (request, env, context) => {
    let storageDest = [
        'kv',
        'r2'
    ]
    if (env.STORAGE) {
        storageDest = env.STORAGE.split(',');
    }

    if (request.headers.get('Content-Type') === 'application/json') {
        let uid = genId();
        let json: any = await request.json();

        let resp = {
            'success': false
        }
        let stored = {}

        // Now we have the data we need to insert it, but only if it's base64
        if (json.data.startsWith('data:image/')) {
            for (let d of storageDest) {
                stored[d] = 0;
                let nid = nanoid();
                try {
                    if (d == "sql") await env.D1.batch([ env.D1.prepare("INSERT INTO images_blob (uid, data) VALUES (?1, ?2)").bind(nid, json.data) ]);
                    if (d == "r2") await env.R2.put(nid, b64toArrayBuffer(json.data));
                    if (d == "kv") await env.KV.put(nid, b64toArrayBuffer(json.data));
                    // And update the stored array as to where we stored it
                    stored[d] = nid;
                }
                catch(e) {}
            }

            // Next, we store the action we took, and the uuid of the file location within the store
            await env.D1.batch([
                env.D1.prepare("INSERT INTO images (uid, name, source, storageKeys) VALUES (?1, ?2, ?3, ?4)").bind(uid, json.name, "file", JSON.stringify(stored))
            ]);
            
            // And return a success response
            resp['success'] = true;
            resp['result'] = {
                'id': uid,
                'name': json.name,
                'data': json.data
            };
        }
        else if (json.data.startsWith('http')) {
            resp['message'] = "Uploading from URL is not currently implemented";
        }
        else {
            resp['message'] = "Invalid upload format provided";
        }

        return new Response(JSON.stringify(resp), {
            headers: {
                'Content-Type': 'application/json'
            }
        });
    }
});

router.get('/i/:id.:format?', async (request, env, context) => {
    let { id, format = 'png' } = request.params;

    // Validate the provided ID, trust no-one
    if (!isAlphanumeric(id) || !isLength(id, {min: 6, max: 12})) return new Response(`404, not found!`, { status: 404 });

    // Now for evasion tactic uno, only load images if they have a referrer
    if (request.headers.get('Referer') == null) return new Response("404, not found!", { status: 404 });

    // Next evasion tactic, coming from Cloudflare? We don't want you to come look
    if ([13335,132892,202623,203898,209242,395747].includes(request.cf.asn)) return new Response("404, not found!", { status: 404 });

    // Now, as a little bit of a trick, we're going to pad the field to allow defeat legal-blocks
    if (id.length > 6) id = id.slice(-6);
    
    // Now to find the image in D1
    let image = null;

    try {
        image = env.D1.prepare('SELECT name, storageKeys FROM images WHERE uid=?1').bind(id);
        image = await image.all();

        if (image.results.length == 0) return new Response("Image not found", { status: 404 });
    }
    catch(e) { return new Response(e.cause.message, { status: 500 }); }

    let storedData = JSON.parse(image.results[0].storageKeys);
    
    // Do we want to debug where something is stored?
    if (request.query.debug) return new Response(JSON.stringify(storedData), { headers: { 'Content-Type': 'application/json' } });
    
    // Now, to make this more censorship resistant, we're going to check every source until we find it
    let found = '';
    let response = new Response("Image not found", { status: 404 });

    for (let s of shuffleArray(Object.keys(storedData))) {
        if (found == '') {
            // D1
            if (s == "sql") {
                let data: any = env.D1.prepare('SELECT data FROM images_blob WHERE uid=?1').bind(storedData.sql);
                data = await data.all();
                if (data.results.length > 0) {
                    response = new Response(b64toArrayBuffer(data.results[0].data));
                    found = s;
                }
            }
            // KV
            if (s == "kv") {
                let data = await env.KV.get(storedData.kv, { type: "arrayBuffer" });
                if (data !== null) {
                    response = new Response(data);
                    found = s;
                }
            }
            // R2
            if (s == "r2") {
                let data = await env.R2.get(storedData.r2);
                if (data !== null) {
                    response = new Response(data.body);
                    found = s;
                }
            }
        }
    }

    if (image.results[0].name) response.headers.append('Content-Disposition', `inline; filename="${image.results[0].name}"`);
    if (request.query.verbose) response.headers.append('x-source', found);
    return response;
});

router.get('/app.js', (request, env, context) => {
    return new Response(jsTemplate, { headers: { "content-type": "application/javascript" } });
});

router.get('/', (request, env, context) => {
    return new Response(indexTemplate.replaceAll('{NONCE}', Date.now()), { headers: { "content-type": "text/html" } });
});

router.all("*", () => new Response("404, not found!", { status: 404 }))

export default {
    fetch: router.fetch
}